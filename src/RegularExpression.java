import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RegularExpression {
    public static void main(String[] args) {
        int[] money = {1, 2, 5, 10, 20};
        List<Integer> chosenValues = new ArrayList<>();
        Random rand = new Random();
        int chosenElement;

        for (int step = 1; true; step++) {
            chosenElement = money[rand.nextInt(money.length)];
            chosenValues.add(chosenElement);
            int listSum = sum(chosenValues);

            if (listSum > 100) {
                chosenValues.remove(new Integer(chosenElement));
                continue;
            }

            System.out.println(step + " " + listSum + " Elements " + chosenValues);

            if (listSum == 100) {
                break;
            }
        }
    }

    public static int sum(List<Integer> list) {
        int sum = 0;
        for (int i : list) {
            sum += i;
        }
        return sum;
    }
}
